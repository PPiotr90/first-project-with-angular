import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {PeopleService} from "../people.service";

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnInit {
  @Output() changedList = new EventEmitter();
  @Output() setFields = new EventEmitter();
  personForm
  personInForm
  idInput
  firstNameInput
  lastNameInput
  ageInput
  peselInput
  postalCodeInput
  cityInput
  streetInput

  inputs


  savePerson() {
    if (!this.personForm.valid) {
      alert(' błędne dane w formularzu')
    } else {
      let personToSave = JSON.stringify(this.personForm.value)
      this.peopleService.savePerson(personToSave).subscribe(res => {
        if (res.status == 201) {
          this.changedList.emit()
          this.clearForm()
        }
      })
    }
  }

  updatePerson() {
    let personToUpdate = this.personForm.value
    personToUpdate = this.fillNullFields(personToUpdate)
    personToUpdate = JSON.stringify(personToUpdate)
    this.peopleService.updatePerson(personToUpdate).subscribe(res => {
      if (res.status == 200) {
        this.changedList.emit()
        this.clearForm()
      }
    })
  }

  fillNullFields(personToFill) {
    personToFill.id = this.personInForm.id.toString()
    if (personToFill.firstName == null) personToFill.firstName = this.personInForm.firstName
    if (personToFill.lastName == null) personToFill.lastName = this.personInForm.lastName
    if (personToFill.age == null) personToFill.age = this.personInForm.age
    if (personToFill.pesel == null) personToFill.pesel = this.personInForm.pesel
    if (personToFill.postalCode == null) personToFill.postalCode = this.personInForm.postalCode
    if (personToFill.city == null) personToFill.city = this.personInForm.city
    if (personToFill.street == null) personToFill.street = this.personInForm.street
    return personToFill;
  }


  clearForm() {
    this.setEmptyPersonInForm()
    this.inputs.forEach(input => {
      input.reset()

    })
    console.log(this.personInForm)
  }

  checkPesel(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value.length === 0) {
      return null
    }
    let potentialPesel = (String)(control.value);
    if (potentialPesel.length > 0 && !(potentialPesel.match('\\d{11}'))) {

      return {'notMatchNumberOfDigits': true}

    } else if (control.value != null) {
      let table = new Array(11);
      for (let i = 0; i < potentialPesel.length; i++) {
        table[i] = Number.parseInt(potentialPesel.charAt(i));
      }
      let checkNumber = table[0] + 3 * table[1] + 7 * table[2] + 9 * table[3] + table[4]
        + 3 * table[5] + 7 * table[6] + 9 * table[7] + table[8] + 3 * table[9] + 1 * table[10];
      if (checkNumber % 10 != 0) {
        return {'notMatchPattern': true}

      }
    }
  };

  checkAge(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value == null || control.value.length === 0) {
      return null
    } else if ((isNaN(control.value)) || control.value > 100 || control.value < 1) {
      return {'outOfRange': true}
    }
    return null
  }

  getFields() {
    this.idInput = this.personForm.get('id')
    this.firstNameInput = this.personForm.get('firstName')
    this.lastNameInput = this.personForm.get('lastName')
    this.ageInput = this.personForm.get('age')
    this.peselInput = this.personForm.get('pesel')
    this.postalCodeInput = this.personForm.get('postalCode')
    this.cityInput = this.personForm.get('city')
    this.streetInput = this.personForm.get('street')

    this.inputs = [
      this.idInput,
      this.firstNameInput,
      this.lastNameInput,
      this.ageInput,
      this.peselInput,
      this.postalCodeInput,
      this.cityInput,
      this.streetInput
    ]

  }

  fillFieldsForPerson(person) {
    this.clearForm()
    this.personInForm = Object.assign({}, person)
    this.idInput.value = person.id;
  }


  setEmptyPersonInForm() {
    this.personInForm = {}
  }


  constructor(private formBuilder: FormBuilder,
              private peopleService: PeopleService) {


    this.personForm = formBuilder.group({
      id: '',
      'firstName': [null, Validators.compose([Validators.required,
        Validators.maxLength(50),
        Validators.pattern('[a-zA-Z]*')])],

      'lastName': [null, Validators.compose([Validators.required,
        Validators.maxLength(100),
        Validators.pattern('[a-zA-Z]*')])],

      'age': [null, this.checkAge],

      'pesel': [null, Validators.compose([this.checkPesel])],

      'postalCode': [null, Validators.pattern('\\d{2}-\\d{3}')],

      'city': [null, Validators.compose([Validators.required,
        Validators.maxLength(50),
        Validators.pattern('(\\w*\\s*)*')])],

      'street': [null, Validators.compose([Validators.required,
        Validators.maxLength(50),
        Validators.pattern('(\\w*\\s*)*')])],

    })
    this.getFields()
    this.setEmptyPersonInForm()

  }

  ngOnInit(): void {

  }


}
