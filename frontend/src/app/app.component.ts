import {Component, ViewChild} from '@angular/core';
import {PersonFormComponent} from "./person-form/person-form.component";
import {PersonListComponent} from "./person-list/person-list.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  @ViewChild(PersonFormComponent) personFormComponent : PersonFormComponent
  @ViewChild(PersonListComponent) personListComponent : PersonListComponent

  sendToForm(value) {
this.personFormComponent.fillFieldsForPerson(value.person)
  }

  refreshList() {
    this.personListComponent.getPeople();

  }
}
