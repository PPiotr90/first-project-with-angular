import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from "@angular/common";


import { AppComponent } from './app.component';
import { PersonListComponent } from './person-list/person-list.component';
import { PersonFormComponent } from './person-form/person-form.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    PersonListComponent,
    PersonFormComponent
  ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
      HttpClientModule,
      CommonModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
