import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PeopleService} from "../people.service";


class Person {

}

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {
  people: Person[] = [];
  @Output() actualPerson = new  EventEmitter();

  getPeople() {
    this.peopleService.getPeopleFromDataBase().subscribe(results => this.people = results);

  }

  deletePerson(id) {
    this.peopleService.deleteById(id).subscribe(res => {
      if (res.status == 204) {
        this.getPeople()
      }
    });
  }

  addPeople() {
    this.peopleService.fillList().subscribe(res => {
      if (res.status == 200) {
        this.getPeople()
      }
    });
  }

  changePerson(personToShow) {


    this.actualPerson.emit({event: Event, person: personToShow})
  }0

  constructor(private peopleService: PeopleService) {
  }

  ngOnInit(): void {
    this.getPeople();
  }

}
