import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";






@Injectable({
  providedIn: 'root'
})
export class PeopleService {
  private address = "http://localhost:8080/people"
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  getPeopleFromDataBase(): Observable<Object[]> {
    return this.httpClient.get<Object[]>(this.address);
  }
  deleteById(id) {

    return this.httpClient.delete((String)(this.address+'/'+id.toString()), {
      headers : this.headers, observe: "response"})
  }

  savePerson(personToSave) {
    return this.httpClient.post(this.address, personToSave, {
      headers : this.headers, observe: "response"})
  }
  updatePerson(personToUpdate) {
    return this.httpClient.patch(this.address, personToUpdate, {
      headers : this.headers, observe: "response"})
  }
  fillList() {
    return this.httpClient.get(this.address+'/fill',{
      headers : this.headers, observe: "response"})
  }
  constructor(private httpClient: HttpClient) {
  }
}
