package pl.pp.backend.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pp.backend.component.DataInitializer;
import pl.pp.backend.dto.PersonDto;
import pl.pp.backend.entity.Person;
import pl.pp.backend.service.PersonService;

import java.util.List;

@RestController
@RequestMapping("/people")
@CrossOrigin("*")
public class PersonController {
    private PersonService personService;

    PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public List<Person> findAll() {
        return personService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person post(@RequestBody PersonDto personDto) {
        return personService.save(personDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Person findById(@PathVariable("id") String id) {
        Long identification = Long.parseLong(id);
        return personService.findById(identification);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") String id) {
        Long identification = Long.parseLong(id);
        personService.deleteById(identification);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public Person patch(@RequestBody Person person) {
        return personService.update(person);
    }

    @GetMapping("/fill")
    public void addPersonToBase() {
        DataInitializer.addPersonToBase();
    }
}
