package pl.pp.backend.dto;

import lombok.Data;

@Data
public class PersonDto {
    private  String firstName;
    private  String lastName;
    private  int age;
    private  String pesel;
    private  String postalCode;
    private  String city;
    private  String street;
}
