package pl.pp.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pp.backend.entity.Person;
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
}
