package pl.pp.backend.component;

import io.codearte.jfairy.Fairy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.pp.backend.dto.PersonDto;
import pl.pp.backend.service.PersonService;

import java.util.Random;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private static PersonService personService;
    private static final Fairy fairy = Fairy.create();
    private   static Random random;

    @Autowired
    DataInitializer(PersonService personService) {
        this.personService = personService;
        random = new Random();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

addPersonToBase();
        }


    private static String postalCode() {
        String postalCode = fairy.person().getAddress().getPostalCode();
        StringBuilder stringBuilder = new StringBuilder(postalCode);
        postalCode = stringBuilder.insert(2, "-").toString();
        return postalCode;
    }
    public  static  void addPersonToBase() {

        for (int i = 0; i < 10; i++) {
            PersonDto personDto = new PersonDto();
            personDto.setFirstName(fairy.person().getFirstName());
            personDto.setLastName(fairy.person().getLastName());
            personDto.setAge(fairy.person().getAge());
            personDto.setPesel(Long.valueOf((long) (random.nextDouble() * 100000000000L)).toString());
            personDto.setPostalCode(postalCode());
            personDto.setCity(fairy.person().getAddress().getCity());
            personDto.setStreet(fairy.person().getAddress().getStreet());
            personService.save(personDto);
    }
}
}
