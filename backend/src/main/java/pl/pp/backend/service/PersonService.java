package pl.pp.backend.service;

import org.springframework.stereotype.Service;
import pl.pp.backend.dto.PersonDto;
import pl.pp.backend.entity.Person;
import pl.pp.backend.repository.PersonRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class PersonService {
    private PersonRepository personRepository;

    PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> findAll() {
        return personRepository.findAll();
    }

    public Person save(PersonDto personDto) {
        Person personToSave = createPersonFromDto(personDto);
        return personRepository.save(personToSave);
    }

    public Person findById(Long id) {
        return personRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("in data base is no Person with this id"));
    }

    public Person update(Person person) {
        return personRepository.save(person);
    }

    public void deleteById(Long id) {
        personRepository.deleteById(id);
    }

    private Person createPersonFromDto(PersonDto source) {
        Person result = new Person();

        result.setFirstName(source.getFirstName());
        result.setLastName(source.getLastName());
        result.setAge(source.getAge());
        result.setPesel(source.getPesel());
        result.setPostalCode(source.getPostalCode());
        result.setCity(source.getCity());
        result.setStreet(source.getStreet());
        return result;
    }
}
